public class Main {
    public static void main(String[] args) {
//        Band 3: upload 1750 - 1770 download 1845-1865
//        Band 7: upload 2510-2520 download 2630 - 2640

        double middleUploadFreq = (double) (2510 + 2520) / 2;
        double middleDownloadFreq = (double) (2630 + 2640) / 2;

        System.out.printf("Upload freq: %.4f\n", middleUploadFreq);
        System.out.printf("Download freq: %.4f\n", middleDownloadFreq);

        Calculator calculator = new Calculator();
        CalculationValuesDTO downloadVals = new CalculationValuesDTO();
        downloadVals.setCoefficient(Constants.COEFFICIENT);
        downloadVals.setHeight_mobile(Constants.HEIGHT_MOBILE);
        downloadVals.setHeight_station(Constants.HEIGHT_STATION);
        downloadVals.setFreq(middleDownloadFreq);
        downloadVals.setL(145);

        double radDownload = calculator.calculateRadius(downloadVals);

        CalculationValuesDTO uploadVals = new CalculationValuesDTO();
        uploadVals.setCoefficient(Constants.COEFFICIENT);
        uploadVals.setHeight_mobile(Constants.HEIGHT_MOBILE);
        uploadVals.setHeight_station(Constants.HEIGHT_STATION);
        uploadVals.setFreq(middleUploadFreq);
        uploadVals.setL(135);
        double radUpload = calculator.calculateRadius(uploadVals);

        System.out.println(radDownload);
        System.out.println(radUpload);
        System.out.println();
        double amount = calculator.calculateAmount(Math.min(radUpload, radDownload));
        System.out.printf("Calculations result is: %.4f.\t Amount of stations: %d\n", amount, (int) Math.ceil(amount));
    }

}