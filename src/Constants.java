public final class Constants {
    public static final double COEFFICIENT = 3d;

    public static final double HEIGHT_STATION = 60d;
    public static final double HEIGHT_MOBILE = 1d;

    public static final double ZONE_AREA = 35;


    public static void main(String[] args) {
        System.out.println(Math.PI * Math.pow(0.6405, 2) * 35);

        System.out.println(45.1 /( Math.PI * Math.pow(0.6405, 2) ));
    }


}
