public class CalculationValuesDTO {
    private double coefficient;
    private double height_station;
    private double height_mobile;
    private double freq;
    private double L;

    public double getCoefficient() {
        return coefficient;
    }

    public void setCoefficient(double coefficient) {
        this.coefficient = coefficient;
    }

    public double getHeight_station() {
        return height_station;
    }

    public void setHeight_station(double height_station) {
        this.height_station = height_station;
    }

    public double getHeight_mobile() {
        return height_mobile;
    }

    public void setHeight_mobile(double height_mobile) {
        this.height_mobile = height_mobile;
    }

    public double getFreq() {
        return freq;
    }

    public void setFreq(double freq) {
        this.freq = freq;
    }

    public double getL() {
        return L;
    }

    public void setL(double l) {
        L = l;
    }

    public CalculationValuesDTO() {
    }

    public CalculationValuesDTO(double coefficient, double height_station, double height_mobile, double freq, double l) {
        this.coefficient = coefficient;
        this.height_station = height_station;
        this.height_mobile = height_mobile;
        this.freq = freq;
        L = l;
    }
}
