public class Calculator {

    public double calculateAmount(double radius) {
        return 1.21 * (Constants.ZONE_AREA / (Math.PI * Math.pow(radius, 2)));
    }

    public double calculateRadius(CalculationValuesDTO values) {
        double A_ab = (1.11 * Math.log10(values.getFreq()) - 0.7) * values.getHeight_mobile() - (1.56 * Math.log10(values.getFreq()) - 0.8);

        double Rad_lg_upper = values.getL() - 46.3 - 33.9 * Math.log10(values.getFreq()) + 13.82 * Math.log10(values.getHeight_station()) + A_ab - values.getCoefficient();
        double Rad_lg_lower = 44.9 - (6.55 * Math.log10(values.getHeight_station()));
        double Rad_lg_div = Rad_lg_upper / Rad_lg_lower;
        double Rad_res = Math.pow(10, Rad_lg_div);


        System.out.println("***************************");
        System.out.printf("Function val:\t %.4f \n", A_ab);
        System.out.printf("Upper fraction val:\t %.4f \n", Rad_lg_upper);
        System.out.printf("Lower fraction val:\t %.4f \n", Rad_lg_lower);
        System.out.printf("Division result:\t %.4f \n", Rad_lg_div);
        System.out.printf("Result is:\t %.4f\n", Rad_res);
        System.out.println("***************************");
        return Rad_res;
    }
}
